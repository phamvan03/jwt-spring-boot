package com.example.jwtspringboot.service;

import com.example.jwtspringboot.exception.JwtSpringBootException;
import com.example.jwtspringboot.model.request.LoginRegisterRequestBody;
import com.example.jwtspringboot.model.response.StatusResponseBody;

public interface UserService {
	StatusResponseBody register(LoginRegisterRequestBody requestBody) throws JwtSpringBootException;
	StatusResponseBody changePassword(String newPassword) throws JwtSpringBootException;
}
