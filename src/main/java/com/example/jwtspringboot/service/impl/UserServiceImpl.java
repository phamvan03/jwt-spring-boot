package com.example.jwtspringboot.service.impl;

import com.example.jwtspringboot.entity.UserEntity;
import com.example.jwtspringboot.exception.JwtSpringBootException;
import com.example.jwtspringboot.model.request.LoginRegisterRequestBody;
import com.example.jwtspringboot.model.response.StatusResponseBody;
import com.example.jwtspringboot.repository.UserRepository;
import com.example.jwtspringboot.service.UserService;
import com.example.jwtspringboot.untils.Common;
import com.example.jwtspringboot.untils.JwtSpringBootErrorCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {
	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;

	@Override
	public StatusResponseBody register(LoginRegisterRequestBody requestBody) throws JwtSpringBootException {
		try {
			StatusResponseBody responseBody = new StatusResponseBody();
			if (userRepository.findByUsername(requestBody.getUsername()) == null) {
				UserEntity user = new UserEntity();

				user.setUsername(requestBody.getUsername());
				user.setPassword(passwordEncoder.encode(requestBody.getPassword()));

				userRepository.save(user);

				responseBody.setStatus(Common.STATUS_REGISTER_SUCCESS);
				return responseBody;
			}

			responseBody.setStatus(Common.STATUS_REGISTER_FAILED);
			return responseBody;
		} catch (Exception e) {
			throw new JwtSpringBootException(JwtSpringBootErrorCode.BAD_REQUEST, "BAD_REQUEST");
		}
	}

	@Override
	public StatusResponseBody changePassword(String newPassword) throws JwtSpringBootException {
		try {

		} catch (Exception e) {
			throw new JwtSpringBootException(JwtSpringBootErrorCode.BAD_REQUEST, "BAD_REQUEST");
		}
		return null;
	}
}
