package com.example.jwtspringboot.service.impl;

import com.example.jwtspringboot.entity.RefreshTokenEntity;
import com.example.jwtspringboot.entity.UserEntity;
import com.example.jwtspringboot.model.request.LogoutRequest;
import com.example.jwtspringboot.repository.RefreshTokenRepository;
import com.example.jwtspringboot.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class RefreshTokenService {
	private final RefreshTokenRepository refreshTokenRepository;
	private final UserRepository userInfoRepository;

	public RefreshTokenEntity createRefreshToken(String username) {
		RefreshTokenEntity refreshToken = RefreshTokenEntity.builder()
			.userInfo(userInfoRepository.findByUsername(username).get())
			.token(UUID.randomUUID().toString())
			.expiryDate(Instant.now().plusMillis(600000 * 1000))//10
			.build();
		return refreshTokenRepository.save(refreshToken);
	}

	public Optional<RefreshTokenEntity> findByToken(String token) {
		return refreshTokenRepository.findByToken(token);
	}

	public RefreshTokenEntity verifyExpiration(RefreshTokenEntity token) {
		if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
			refreshTokenRepository.delete(token);
			throw new RuntimeException(token.getToken() + " Refresh token was expired. Please make a new signin request");
		}
		return token;
	}

	public String logout(LogoutRequest request) {
		UserEntity userEntity = userInfoRepository.findById(request.getUserId()).get();

		List<RefreshTokenEntity> refreshTokens = refreshTokenRepository.findAllByUserInfo(userEntity);

		refreshTokenRepository.deleteAll(refreshTokens);

		return "logout ok";
	}
}
