package com.example.jwtspringboot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class TokenBlacklistService {
	private final RedisTemplate<String, String> redisTemplate;

	private static final String BLACKLIST_PREFIX = "blacklist:";

	@Autowired
	public TokenBlacklistService(RedisTemplate<String, String> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	public void addToBlacklist(String token) {
		redisTemplate.opsForValue().set(BLACKLIST_PREFIX + token, "true");
	}

	public boolean isBlacklisted(String token) {
		return Boolean.TRUE.equals(redisTemplate.hasKey(BLACKLIST_PREFIX + token));
	}

	public void deleteAllData() {
		redisTemplate.execute((RedisCallback<Object>) connection -> {
			connection.flushAll();
			return null;
		});
	}
}
