package com.example.jwtspringboot.exception;

import com.example.jwtspringboot.untils.JwtSpringBootErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class JwtSpringBootExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = JwtSpringBootException.class)
    public ResponseEntity<Object> handleSunnyAdminException(JwtSpringBootException ex, WebRequest request) {
        String errorMessage = ex.getMessage();
        String errorCode = ex.getStatusCode();
        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        if (JwtSpringBootErrorCode.INTERNAL_SERVER_ERROR.equals(errorCode)) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return ResponseEntity.status(httpStatus).body(new JwtSpringBootException(errorCode, errorMessage));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAllOtherExceptions(Exception ex, WebRequest request) {
//        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
        if (ex instanceof IllegalArgumentException) {
            httpStatus = HttpStatus.BAD_REQUEST;
        } else if (ex instanceof NullPointerException) {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return ResponseEntity.status(httpStatus).body(ex.getMessage());
    }
}
