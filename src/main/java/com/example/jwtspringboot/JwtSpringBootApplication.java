package com.example.jwtspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class JwtSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtSpringBootApplication.class, args);
	}

}
