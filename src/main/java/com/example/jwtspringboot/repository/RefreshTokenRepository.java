package com.example.jwtspringboot.repository;

import com.example.jwtspringboot.entity.RefreshTokenEntity;
import com.example.jwtspringboot.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshTokenEntity, Long> {
	Optional<RefreshTokenEntity> findByToken(String token);

	List<RefreshTokenEntity> findAllByUserInfo(UserEntity userEntity);
}
