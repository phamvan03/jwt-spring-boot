package com.example.jwtspringboot.untils;

public interface Common {
	String HEADER_AUTHORIZATION = "Authorization";
	String STATUS_SUCCESS = "SUCCESS";
	String STATUS_REGISTER_SUCCESS = "STATUS_REGISTER_SUCCESS";
	String STATUS_REGISTER_FAILED = "USERNAME_IS_EXISTING";
	String STATUS_FAILED = "FAILED";
	String STATUS_ACTIVE = "ACTIVE";
	String STATUS_INACTIVE = "INACTIVE";
}
