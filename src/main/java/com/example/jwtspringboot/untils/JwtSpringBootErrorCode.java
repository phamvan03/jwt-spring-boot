package com.example.jwtspringboot.untils;

public interface JwtSpringBootErrorCode {
    String INTERNAL_SERVER_ERROR = "SUNNY_ADMIN_500";
    String BAD_REQUEST="SUNNY_ADMIN_400";
    String DATA_NOT_FOUND ="SUNNY_ADMIN_404";
}
