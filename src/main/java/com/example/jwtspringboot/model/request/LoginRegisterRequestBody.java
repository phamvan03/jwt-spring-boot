package com.example.jwtspringboot.model.request;

import lombok.Data;

@Data
public class LoginRegisterRequestBody {
	private String username;
	private String password;
}
