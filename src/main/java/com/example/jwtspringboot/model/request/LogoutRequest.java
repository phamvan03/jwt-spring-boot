package com.example.jwtspringboot.model.request;

import lombok.Data;

@Data
public class LogoutRequest {
	private Long userId;
}
