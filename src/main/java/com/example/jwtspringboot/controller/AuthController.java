package com.example.jwtspringboot.controller;

import com.example.jwtspringboot.entity.RefreshTokenEntity;
import com.example.jwtspringboot.model.RefreshToken;
import com.example.jwtspringboot.model.request.LoginRegisterRequestBody;
import com.example.jwtspringboot.model.request.LogoutRequest;
import com.example.jwtspringboot.model.response.JwtResponseBody;
import com.example.jwtspringboot.model.response.StatusResponseBody;
import com.example.jwtspringboot.service.UserService;
import com.example.jwtspringboot.service.impl.RefreshTokenService;
import com.example.jwtspringboot.service.impl.TokenBlacklistService;
import com.example.jwtspringboot.untils.Common;
import com.example.jwtspringboot.untils.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
	private final UserService userService;
	private final AuthenticationManager authenticationManager;
	private final JwtTokenUtil jwtTokenUtil;
	private final RefreshTokenService refreshTokenService;
	private final TokenBlacklistService tokenBlacklistService;

	@PostMapping("/register")
	public ResponseEntity<StatusResponseBody> register(@RequestBody LoginRegisterRequestBody requestBody) throws Exception {
		return ResponseEntity.ok(userService.register(requestBody));
	}

	@PostMapping("/login")
	public JwtResponseBody authenticateAndGetToken(@RequestBody LoginRegisterRequestBody authRequest) {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
		if (authentication.isAuthenticated()) {
			RefreshTokenEntity refreshToken = refreshTokenService.createRefreshToken(authRequest.getUsername());
			return JwtResponseBody.builder()
				.accessToken(jwtTokenUtil.generateToken(authRequest.getUsername()))
				.token(refreshToken.getToken()).build();
		} else {
			throw new UsernameNotFoundException("invalid user request !");
		}
	}

	@PostMapping("/refreshToken")
	public JwtResponseBody refreshToken(@RequestBody RefreshToken refreshTokenRequest) {
		return refreshTokenService.findByToken(refreshTokenRequest.getRefreshToken())
			.map(refreshTokenService::verifyExpiration)
			.map(RefreshTokenEntity::getUserInfo)
			.map(userInfo -> {
				String accessToken = jwtTokenUtil.generateToken(userInfo.getUsername());
				return JwtResponseBody.builder()
					.accessToken(accessToken)
					.token(refreshTokenRequest.getRefreshToken())
					.build();
			}).orElseThrow(() -> new RuntimeException(
				"Refresh token is not in database!"));
	}

	@GetMapping("/logout")
	public ResponseEntity<String> logout(@RequestBody LogoutRequest request, HttpServletRequest requestHeader) {
		final String requestTokenHeader = requestHeader.getHeader(Common.HEADER_AUTHORIZATION);
		String jwtToken = requestTokenHeader.substring(7);

		tokenBlacklistService.addToBlacklist(jwtToken);

		return ResponseEntity.ok(refreshTokenService.logout(request));
	}

	@GetMapping()
	public String test() {
		tokenBlacklistService.deleteAllData();
		return "ok";
	}
}
