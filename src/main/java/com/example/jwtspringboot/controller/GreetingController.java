package com.example.jwtspringboot.controller;

import com.example.jwtspringboot.exception.JwtSpringBootException;
import com.example.jwtspringboot.untils.JwtSpringBootErrorCode;
import io.jsonwebtoken.JwtException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/greeting")
public class GreetingController {
	@PostMapping("/hello")
	public String hello() {
		return "Hello";
	}

	@PostMapping("/goodbye")
	public String goodbye() throws JwtSpringBootException {
		try {
			return "Goodbye";
		} catch (Exception e) {
			throw new JwtSpringBootException(JwtSpringBootErrorCode.INTERNAL_SERVER_ERROR, "INTERNAL_SERVER_ERROR");
		}
	}
}
